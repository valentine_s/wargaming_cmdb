package net.wargaming.cmdb.orm.repository;

import net.wargaming.cmdb.orm.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

}
