package net.wargaming.cmdb.orm.repository;

import net.wargaming.cmdb.orm.model.Document;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedNativeQuery;

@Repository
public interface DocumentRepository extends CrudRepository<Document, Long> {
  Iterable<Document> findByUserId(Long userId);

  @Query(value = "SELECT * FROM document AS d1 WHERE (" +
    "  SELECT count(*) FROM document AS d2" +
    "    WHERE" +
    "      d1.number = d2.number AND" +
    "      d1.type = d2.type" +
    ") > 1;", nativeQuery = true)
  Iterable<Document> findDuplicates();
}
