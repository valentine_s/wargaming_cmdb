package net.wargaming.cmdb.orm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "user")
public class User extends BaseEntity {

  @Column(name = "name")
  private String name;

  @OneToMany(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private Collection<Document> documents;

  public User() {
  }

  public User(Long id, String name, Collection<Document> documents) {
    this.id = id;
    this.name = name;
    this.documents = documents;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Collection<Document> getDocuments() {
    return documents;
  }

  public void setDocuments(Collection<Document> documents) {
    this.documents = documents;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    User user = (User) o;

    if (id != null ? !id.equals(user.id) : user.id != null) return false;
    if (name != null ? !name.equals(user.name) : user.name != null) return false;
    return documents != null ? documents.equals(user.documents) : user.documents == null;
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (id != null ? id.hashCode() : 0);
    result = 31 * result + (documents != null ? documents.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "User{" +
      "name='" + name + '\'' +
      ", documents=" + documents +
      ", id=" + id +
      '}';
  }
}
