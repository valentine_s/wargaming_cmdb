package net.wargaming.cmdb.orm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "document")
public class Document extends BaseEntity {

  @Column(name = "type")
  private String type;

  @Column(name = "number")
  private String number;

  @Column(name = "user_id")
  private Long userId;

  public Document() {
  }

  public Document(Long id, String type, String number, Long userId) {
    this.id = id;
    this.type = type;
    this.number = number;
    this.userId = userId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Document document = (Document) o;

    if (id != null ? !id.equals(document.id) : document.id != null) return false;
    if (type != null ? !type.equals(document.type) : document.type != null) return false;
    if (number != null ? !number.equals(document.number) : document.number != null) return false;
    return userId != null ? userId.equals(document.userId) : document.userId == null;
  }

  @Override
  public int hashCode() {
    int result = type != null ? type.hashCode() : 0;
    result = 31 * result + (id != null ? id.hashCode() : 0);
    result = 31 * result + (number != null ? number.hashCode() : 0);
    result = 31 * result + (userId != null ? userId.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Document{" +
      "type='" + type + '\'' +
      ", number='" + number + '\'' +
      ", userId=" + userId +
      ", id=" + id +
      '}';
  }
}
