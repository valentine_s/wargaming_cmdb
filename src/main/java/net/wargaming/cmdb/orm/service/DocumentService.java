package net.wargaming.cmdb.orm.service;

import net.wargaming.cmdb.orm.dto.DocumentDto;

import java.util.List;

public interface DocumentService extends BaseService<DocumentDto> {
  List<DocumentDto> getByUserId(Long userId);
  List<DocumentDto> getDuplicates();
}
