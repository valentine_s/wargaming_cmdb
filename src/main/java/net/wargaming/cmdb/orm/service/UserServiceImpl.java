package net.wargaming.cmdb.orm.service;

import net.wargaming.cmdb.orm.dto.UserDto;
import net.wargaming.cmdb.orm.model.User;
import net.wargaming.cmdb.orm.repository.DocumentRepository;
import net.wargaming.cmdb.orm.repository.UserRepository;
import net.wargaming.cmdb.util.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
  private Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private DocumentRepository documentRepository;

  @Override
  @Transactional(readOnly = true)
  public Iterable<UserDto> getAll() {
    LOG.info("Get all users");

    return Converter.getUserDto(userRepository.findAll());
  }

  @Override
  @Transactional
  public void delete(Long id) {
    LOG.info("Delete user by id: " + id);

    userRepository.delete(id);
  }

  @Override
  @Transactional(readOnly = true)
  public UserDto getById(Long id) {
    LOG.info("Get user by id: " + id);

    return Converter.getUserDto(userRepository.findOne(id));
  }

  @Override
  @Transactional
  public UserDto insert(UserDto one) {
    LOG.info("Insert new user with name: " + one.getName());

    return Converter.getUserDto(userRepository.save(Converter.getUser(one)));
  }

  @Override
  @Transactional
  public UserDto update(UserDto one) {
    LOG.info("Update user with id: " + one.getId());

    return Converter.getUserDto(userRepository.save(Converter.getUser(one)));
  }

  @Override
  @Transactional
  public void deleteWithDocs(Long id) {
    LOG.info("Delete user with documents by id: " + id);

    User user = userRepository.findOne(id);
    documentRepository.delete(user.getDocuments());
    userRepository.delete(user.getId());
  }
}
