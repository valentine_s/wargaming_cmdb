package net.wargaming.cmdb.orm.service;

import net.wargaming.cmdb.orm.dto.DocumentDto;
import net.wargaming.cmdb.orm.repository.DocumentRepository;
import net.wargaming.cmdb.util.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.rmi.runtime.Log;

import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {
  private Logger LOG = LoggerFactory.getLogger(DocumentServiceImpl.class);

  @Autowired
  private DocumentRepository repository;

  @Override
  @Transactional(readOnly = true)
  public Iterable<DocumentDto> getAll() {
    LOG.info("Get all documents from db");

    return Converter.getDocumentDto(repository.findAll());
  }

  @Override
  @Transactional
  public void delete(Long id) {
    LOG.info("Delete document with id: " + id);

    repository.delete(id);
  }

  @Override
  @Transactional(readOnly = true)
  public DocumentDto getById(Long id) {
    LOG.info("Get document by id: " + id);

    return Converter.getDocumentDto(repository.findOne(id));
  }

  @Override
  @Transactional
  public DocumentDto insert(DocumentDto document) {
    LOG.info("Insert new document. Number: " + document.getNumber() + "; Type: " + document.getType());

    return Converter.getDocumentDto(repository.save(Converter.getDocument(document)));
  }

  @Override
  @Transactional
  public DocumentDto update(DocumentDto document) {
    LOG.info("Update document with id: " + document.getId());

    return Converter.getDocumentDto(repository.save(Converter.getDocument(document)));
  }

  @Override
  @Transactional(readOnly = true)
  public List<DocumentDto> getByUserId(Long userId) {
    LOG.info("Get documents by userId: " + userId);

    return Converter.getDocumentDto(repository.findByUserId(userId));
  }

  @Override
  @Transactional(readOnly = true)
  public List<DocumentDto> getDuplicates() {
    LOG.info("Get duplicate documents");

    return Converter.getDocumentDto(repository.findDuplicates());
  }
}
