package net.wargaming.cmdb.orm.service;

import net.wargaming.cmdb.orm.dto.UserDto;

public interface UserService extends BaseService<UserDto> {
  void deleteWithDocs(Long id);
}
