package net.wargaming.cmdb.orm.service;

public interface BaseService<T> {
  Iterable<T> getAll();

  void delete(Long id);

  T getById(Long id);

  T insert(T one);

  T update(T one);
}
