package net.wargaming.cmdb.orm.dto;

import java.util.Collection;

public class UserDto {

  private Long id;
  private String name;
  private Collection<DocumentDto> documents;

  public UserDto() {
  }

  public UserDto(Long id, String name, Collection<DocumentDto> documents) {
    this.id = id;
    this.name = name;
    this.documents = documents;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Collection<DocumentDto> getDocuments() {
    return documents;
  }

  public void setDocuments(Collection<DocumentDto> documents) {
    this.documents = documents;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    UserDto userDto = (UserDto) o;

    if (id != null ? !id.equals(userDto.id) : userDto.id != null) return false;
    if (name != null ? !name.equals(userDto.name) : userDto.name != null) return false;
    return documents != null ? documents.equals(userDto.documents) : userDto.documents == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (documents != null ? documents.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "UserDto{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", documents=" + documents +
      '}';
  }
}
