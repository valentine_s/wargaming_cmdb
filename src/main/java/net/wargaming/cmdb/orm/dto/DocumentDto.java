package net.wargaming.cmdb.orm.dto;

public class DocumentDto {

  private Long id;
  private String type;
  private String number;
  private Long userId;

  public DocumentDto() {
  }

  public DocumentDto(Long id, String type, String number, Long userId) {
    this.id = id;
    this.type = type;
    this.number = number;
    this.userId = userId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    DocumentDto that = (DocumentDto) o;

    if (id != null ? !id.equals(that.id) : that.id != null) return false;
    if (type != null ? !type.equals(that.type) : that.type != null) return false;
    if (number != null ? !number.equals(that.number) : that.number != null) return false;
    return userId != null ? userId.equals(that.userId) : that.userId == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (type != null ? type.hashCode() : 0);
    result = 31 * result + (number != null ? number.hashCode() : 0);
    result = 31 * result + (userId != null ? userId.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DocumentDto{" +
      "id=" + id +
      ", type='" + type + '\'' +
      ", number='" + number + '\'' +
      ", userId=" + userId +
      '}';
  }
}
