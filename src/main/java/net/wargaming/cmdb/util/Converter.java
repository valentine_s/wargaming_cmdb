package net.wargaming.cmdb.util;

import net.wargaming.cmdb.orm.dto.DocumentDto;
import net.wargaming.cmdb.orm.dto.UserDto;
import net.wargaming.cmdb.orm.model.Document;
import net.wargaming.cmdb.orm.model.User;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Converter {

  public static User getUser(UserDto userDto) {
    if (userDto != null) {
      return new User(
        userDto.getId(),
        userDto.getName(),
        getDocument(userDto.getDocuments())
      );
    } else {
      return null;
    }
  }

  public static Document getDocument(DocumentDto documentDto) {
    if (documentDto != null) {
      return new Document(
        documentDto.getId(),
        documentDto.getType(),
        documentDto.getNumber(),
        documentDto.getUserId()
      );
    } else {
      return null;
    }
  }

  public static UserDto getUserDto(User user) {
    if (user != null) {
      return new UserDto(
        user.getId(),
        user.getName(),
        getDocumentDto(user.getDocuments())
      );
    } else {
      return null;
    }
  }

  public static DocumentDto getDocumentDto(Document document) {
    if (document != null) {
      return new DocumentDto(
        document.getId(),
        document.getType(),
        document.getNumber(),
        document.getUserId()
      );
    } else {
      return null;
    }
  }

  public static List<User> getUser(Iterable<UserDto> dtos) {
    if (dtos != null) {
      return StreamSupport.stream(dtos.spliterator(), false).map(Converter::getUser).collect(Collectors.toList());
    }
    return null;
  }

  public static List<Document> getDocument(Iterable<DocumentDto> dtos) {
    if (dtos != null) {
      return StreamSupport.stream(dtos.spliterator(), false).map(Converter::getDocument).collect(Collectors.toList());
    }
    return null;
  }

  public static List<UserDto> getUserDto(Iterable<User> users) {
    if (users != null) {
      return StreamSupport.stream(users.spliterator(), false).map(Converter::getUserDto).collect(Collectors.toList());
    }
    return null;
  }

  public static List<DocumentDto> getDocumentDto(Iterable<Document> documents) {
    if (documents != null) {
      return StreamSupport.stream(documents.spliterator(), false).map(Converter::getDocumentDto).collect(Collectors.toList());
    }
    return null;
  }
}
