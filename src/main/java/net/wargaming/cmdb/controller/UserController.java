package net.wargaming.cmdb.controller;

import net.wargaming.cmdb.orm.dto.DocumentDto;
import net.wargaming.cmdb.orm.dto.UserDto;
import net.wargaming.cmdb.orm.service.DocumentService;
import net.wargaming.cmdb.orm.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static net.wargaming.cmdb.controller.Constants.ATTR_ACTION;
import static net.wargaming.cmdb.controller.Constants.ATTR_ERROR_MESSAGE;
import static net.wargaming.cmdb.controller.Constants.ATTR_ID;
import static net.wargaming.cmdb.controller.Constants.ATTR_OBJECT;
import static net.wargaming.cmdb.controller.Constants.ATTR_USER;
import static net.wargaming.cmdb.controller.Constants.ATTR_USERS;
import static net.wargaming.cmdb.controller.Constants.PAGE_CREATE_USER;
import static net.wargaming.cmdb.controller.Constants.PAGE_REMOVE_USER;
import static net.wargaming.cmdb.controller.Constants.PAGE_USER;
import static net.wargaming.cmdb.controller.Constants.PAGE_USERS;
import static net.wargaming.cmdb.controller.Constants.PAGE_USER_DETAILS;
import static net.wargaming.cmdb.controller.Constants.PARAM_ID;
import static net.wargaming.cmdb.controller.Constants.REDIRECT_TO;
import static net.wargaming.cmdb.controller.Constants.VIEW_CONFIRM_REMOVE;
import static net.wargaming.cmdb.controller.Constants.VIEW_CREATE_USER;
import static net.wargaming.cmdb.controller.Constants.VIEW_ERROR;
import static net.wargaming.cmdb.controller.Constants.VIEW_USERS;
import static net.wargaming.cmdb.controller.Constants.VIEW_USER_DETAILS;

@Controller
public class UserController {
  private Logger LOG = LoggerFactory.getLogger(UserController.class);

  @Autowired
  private UserService userService;

  @Autowired
  private DocumentService documentService;

  @GetMapping(PAGE_USER_DETAILS)
  public String getUserById(@PathVariable(name = PARAM_ID) Long id, Model model) {
    LOG.info("Display user details with id: " + id);

    if (id != null) {
      UserDto user = userService.getById(id);

      if (user != null) {
        model.addAttribute(ATTR_USER, user);
      } else {
        model.addAttribute(ATTR_ERROR_MESSAGE, "User with id: " + id + " not found!");
        return VIEW_ERROR;
      }
    } else {
      model.addAttribute(ATTR_ERROR_MESSAGE, "User id not found!");
      return VIEW_ERROR;
    }

    return VIEW_USER_DETAILS;
  }

  @PostMapping(PAGE_USER)
  public String updateUser(@ModelAttribute(value = ATTR_USER) UserDto user) {
    if (user != null) {
      LOG.info("Action update user with id: " + user.getId());

      List<DocumentDto> docs = documentService.getByUserId(user.getId());
      user.setDocuments(docs);
      userService.update(user);
    }

    return REDIRECT_TO + PAGE_USERS;
  }

  @GetMapping(PAGE_USERS)
  public String getUsers(Model model) {
    LOG.info("Display users list");

    model.addAttribute(ATTR_USERS, userService.getAll());

    return VIEW_USERS;
  }

  @GetMapping(PAGE_CREATE_USER)
  public String createUserView(Model model) {
    LOG.info("Display page user_create");

    model.addAttribute(ATTR_USER, new UserDto());

    return VIEW_CREATE_USER;
  }

  @PostMapping(PAGE_CREATE_USER)
  public String createUser(@ModelAttribute(value = ATTR_USER) UserDto user) {
    if (user != null) {
      LOG.info("Action create new user: " + user.getName());
      userService.insert(user);
    }

    return REDIRECT_TO + PAGE_USERS;
  }

  @GetMapping(PAGE_REMOVE_USER)
  public String confirmRemoveUser(@PathVariable Long id,
                                  @RequestParam(defaultValue = "false") Boolean withDocs,
                                  HttpServletRequest request,
                                  Model model) {
    if (id != null) {
      LOG.info("Display page confirm remove user with id: " + id);

      String uri = request.getRequestURI();
      if (withDocs) {
        uri += "?withDocs=true";
      }
      model.addAttribute(ATTR_ACTION, uri);
      model.addAttribute(ATTR_OBJECT, "user");
      model.addAttribute(ATTR_ID, id);
    } else {
      model.addAttribute(ATTR_ERROR_MESSAGE, "User id not found!");
      return VIEW_ERROR;
    }

    return VIEW_CONFIRM_REMOVE;
  }

  @PostMapping(PAGE_REMOVE_USER)
  public String removeUser(@PathVariable Long id,
                           @RequestParam(defaultValue = "false") Boolean withDocs,
                           Model model) {
    if (id != null) {
      LOG.info("Action remove user with id: " + id);

      if (withDocs) {
        userService.deleteWithDocs(id);
      } else {
        userService.delete(id);
      }
    } else {
      model.addAttribute(ATTR_ERROR_MESSAGE, "User id not found!");
      return VIEW_ERROR;
    }

    return REDIRECT_TO + PAGE_USERS;
  }

}
