package net.wargaming.cmdb.controller;

import net.wargaming.cmdb.orm.dto.DocumentDto;
import net.wargaming.cmdb.orm.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

import static net.wargaming.cmdb.controller.Constants.ATTR_ACTION;
import static net.wargaming.cmdb.controller.Constants.ATTR_DOC;
import static net.wargaming.cmdb.controller.Constants.ATTR_DOCS;
import static net.wargaming.cmdb.controller.Constants.ATTR_DOCS_DUPLICATES;
import static net.wargaming.cmdb.controller.Constants.ATTR_ERROR_MESSAGE;
import static net.wargaming.cmdb.controller.Constants.ATTR_ID;
import static net.wargaming.cmdb.controller.Constants.ATTR_OBJECT;
import static net.wargaming.cmdb.controller.Constants.PAGE_CREATE_DOC;
import static net.wargaming.cmdb.controller.Constants.PAGE_DOC;
import static net.wargaming.cmdb.controller.Constants.PAGE_DOCS;
import static net.wargaming.cmdb.controller.Constants.PAGE_DOC_DETAILS;
import static net.wargaming.cmdb.controller.Constants.PAGE_REMOVE_DOC;
import static net.wargaming.cmdb.controller.Constants.PARAM_ID;
import static net.wargaming.cmdb.controller.Constants.REDIRECT_TO;
import static net.wargaming.cmdb.controller.Constants.VIEW_CONFIRM_REMOVE;
import static net.wargaming.cmdb.controller.Constants.VIEW_CREATE_DOC;
import static net.wargaming.cmdb.controller.Constants.VIEW_DOCS;
import static net.wargaming.cmdb.controller.Constants.VIEW_DOC_DETAILS;
import static net.wargaming.cmdb.controller.Constants.VIEW_ERROR;

@Controller
public class DocumentController {
  private Logger LOG = LoggerFactory.getLogger(DocumentController.class);

  @Autowired
  private DocumentService documentService;

  @GetMapping(PAGE_DOC_DETAILS)
  public String getDocById(@PathVariable(name = PARAM_ID) Long id, Model model) {

    if (id != null) {
      LOG.info("Display document details page. Doc id: " + id);
      DocumentDto doc = documentService.getById(id);

      if (doc != null) {
        model.addAttribute(ATTR_DOC, doc);
      } else {
        model.addAttribute(ATTR_ERROR_MESSAGE, "Document with id: " + id + " not found!");
        return VIEW_ERROR;
      }
    } else {
      model.addAttribute(ATTR_ERROR_MESSAGE, "Document id not found!");
      return VIEW_ERROR;
    }

    return VIEW_DOC_DETAILS;
  }

  @GetMapping(PAGE_DOCS)
  public String getDocs(Model model) {
    LOG.info("Display document list page");

    model.addAttribute(ATTR_DOCS, documentService.getAll());
    model.addAttribute(ATTR_DOCS_DUPLICATES, documentService.getDuplicates());

    return VIEW_DOCS;
  }

  @PostMapping(PAGE_DOC)
  public String updateDoc(@ModelAttribute(value = ATTR_DOC) DocumentDto doc) {
    if (doc != null) {
      LOG.info("Action update doc with id: " + doc.getId());
      documentService.update(doc);
    }

    return REDIRECT_TO + PAGE_DOCS;
  }

  @GetMapping(PAGE_CREATE_DOC)
  public String createDocView(Model model) {
    LOG.info("Display page doc_create");

    model.addAttribute(ATTR_DOC, new DocumentDto());

    return VIEW_CREATE_DOC;
  }

  @PostMapping(PAGE_CREATE_DOC)
  public String createDoc(@ModelAttribute(value = ATTR_DOC) DocumentDto doc) {
    if (doc != null) {
      LOG.info("Action create new doc: " + doc.getNumber());
      documentService.insert(doc);
    }

    return REDIRECT_TO + PAGE_DOCS;
  }

  @GetMapping(PAGE_REMOVE_DOC)
  public String confirmRemoveDoc(@PathVariable Long id,
                                 HttpServletRequest request,
                                 Model model) {
    if (id != null) {
      LOG.info("Display page confirm remove doc with id: " + id);

      String uri = request.getRequestURI();
      model.addAttribute(ATTR_ACTION, uri);
      model.addAttribute(ATTR_OBJECT, "document");
      model.addAttribute(ATTR_ID, id);
    } else {
      model.addAttribute(ATTR_ERROR_MESSAGE, "Document id not found!");
      return VIEW_ERROR;
    }

    return VIEW_CONFIRM_REMOVE;
  }

  @PostMapping(PAGE_REMOVE_DOC)
  public String removeDoc(@PathVariable Long id, Model model) {
    if (id != null) {
      LOG.info("Action remove document with id: " + id);
      documentService.delete(id);
    } else {
      model.addAttribute(ATTR_ERROR_MESSAGE, "Document id not found!");
      return VIEW_ERROR;
    }

    return REDIRECT_TO + PAGE_DOCS;
  }

}
