package net.wargaming.cmdb.controller;

import net.wargaming.cmdb.orm.dto.DocumentDto;
import net.wargaming.cmdb.orm.dto.UserDto;
import net.wargaming.cmdb.orm.service.DocumentService;
import net.wargaming.cmdb.orm.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static net.wargaming.cmdb.controller.Constants.ATTR_ERROR_MESSAGE;
import static net.wargaming.cmdb.controller.Constants.PAGE_403;
import static net.wargaming.cmdb.controller.Constants.PAGE_ERROR;
import static net.wargaming.cmdb.controller.Constants.PAGE_HOME;
import static net.wargaming.cmdb.controller.Constants.PAGE_LOGIN;
import static net.wargaming.cmdb.controller.Constants.VIEW_403;
import static net.wargaming.cmdb.controller.Constants.VIEW_ERROR;
import static net.wargaming.cmdb.controller.Constants.VIEW_HOME;
import static net.wargaming.cmdb.controller.Constants.VIEW_LOGIN;

@Controller
public class MainController {
  private Logger LOG = LoggerFactory.getLogger(MainController.class);

  @Autowired
  private UserService userService;

  @Autowired
  private DocumentService documentService;

  @PostConstruct
  public void initIt() throws Exception {
    initDb();
  }

  @GetMapping(PAGE_HOME)
  public String home() {
    LOG.info("Render home page");

    return VIEW_HOME;
  }

  @GetMapping(PAGE_LOGIN)
  public String login() {
    LOG.info("Render login page");

    return VIEW_LOGIN;
  }

  @GetMapping(PAGE_403)
  public String error403() {
    LOG.info("Render error 403 page");

    return VIEW_403;
  }

  @GetMapping(PAGE_ERROR)
  public String error500(Model model) {
    LOG.info("Render error 500 page");

    model.addAttribute(ATTR_ERROR_MESSAGE, "Something went wrong...");

    return VIEW_ERROR;
  }

  private void initDb() {
    LOG.info("Initializing DB");

    UserDto userDto1 = userService.insert(new UserDto(null, "Vova", null));
    UserDto userDto2 = userService.insert(new UserDto(null, "Masha", null));
    UserDto userDto3 = userService.insert(new UserDto(null, "Dima", null));

    DocumentDto documentDto1 = documentService.insert(new DocumentDto(null, "id_card", "122", userDto1.getId()));
    DocumentDto documentDto2 = documentService.insert(new DocumentDto(null, "passport", "133", userDto1.getId()));
    DocumentDto documentDto3 = documentService.insert(new DocumentDto(null, "custom_type", "245", userDto2.getId()));
    DocumentDto documentDto4 = documentService.insert(new DocumentDto(null, "passport", "897", userDto2.getId()));
    DocumentDto documentDto5 = documentService.insert(new DocumentDto(null, "custom_type", "245", userDto2.getId()));
    DocumentDto documentDto6 = documentService.insert(new DocumentDto(null, "travel_card", "555", userDto3.getId()));
    DocumentDto documentDto7 = documentService.insert(new DocumentDto(null, "passport", "999", userDto3.getId()));
    DocumentDto documentDto8 = documentService.insert(new DocumentDto(null, "id_card", "122", userDto3.getId()));

    List<DocumentDto> docs1 = new ArrayList<>();
    docs1.add(documentDto1);
    docs1.add(documentDto2);

    List<DocumentDto> docs2 = new ArrayList<>();
    docs2.add(documentDto3);
    docs2.add(documentDto4);
    docs2.add(documentDto5);

    List<DocumentDto> docs3 = new ArrayList<>();
    docs3.add(documentDto6);
    docs3.add(documentDto7);
    docs3.add(documentDto8);

    userDto1.setDocuments(docs1);
    userDto2.setDocuments(docs2);
    userDto3.setDocuments(docs3);

    userService.update(userDto1);
    userService.update(userDto2);
    userService.update(userDto3);
  }

}
