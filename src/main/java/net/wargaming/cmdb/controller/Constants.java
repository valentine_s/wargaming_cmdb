package net.wargaming.cmdb.controller;

public class Constants {
  public static final String ATTR_USERS = "userList";
  public static final String ATTR_USER = "user";
  public static final String ATTR_DOCS = "docList";
  public static final String ATTR_DOC = "doc";
  public static final String ATTR_DOCS_DUPLICATES = "docDuplicates";
  public static final String ATTR_ERROR_MESSAGE = "errorMessage";
  public static final String ATTR_ACTION = "action";
  public static final String ATTR_OBJECT = "object";
  public static final String ATTR_ID = "id";

  public static final String PARAM_ID = "id";

  public static final String PAGE_HOME = "/";
  public static final String PAGE_LOGIN = "/login";
  public static final String PAGE_USERS = "/users";
  public static final String PAGE_USER = "/user";
  public static final String PAGE_USER_DETAILS = "/user/{id}";
  public static final String PAGE_CREATE_USER = "/user/new";
  public static final String PAGE_REMOVE_USER = "/user/{id}/remove";
  public static final String PAGE_DOCS = "/docs";
  public static final String PAGE_DOC = "/doc";
  public static final String PAGE_DOC_DETAILS = "/doc/{id}";
  public static final String PAGE_CREATE_DOC = "/doc/new";
  public static final String PAGE_REMOVE_DOC = "/doc/{id}/remove";
  public static final String PAGE_403 = "/403";
  public static final String PAGE_ERROR = "/error";

  public static final String VIEW_HOME = "home";
  public static final String VIEW_LOGIN = "login";
  public static final String VIEW_USERS = "users";
  public static final String VIEW_USER_DETAILS = "user_details";
  public static final String VIEW_CREATE_USER = "user_create";
  public static final String VIEW_DOCS = "documents";
  public static final String VIEW_DOC_DETAILS = "document_details";
  public static final String VIEW_CREATE_DOC = "document_create";
  public static final String VIEW_ERROR = "error/500";
  public static final String VIEW_CONFIRM_REMOVE = "confirm_remove";
  public static final String VIEW_403 = "error/403";

  public static final String REDIRECT_TO = "redirect:";
}
