package net.wargaming.cmdb.repository;

import net.wargaming.cmdb.orm.model.User;
import net.wargaming.cmdb.orm.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class UserRepositoryTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private UserRepository userRepository;

  @Test
  public void testFindById() {

    User user = new User();
    user.setName("Vova");
    user = entityManager.persist(user);

    User gettedUser = userRepository.findOne(user.getId());

    assertEquals(user.getId(), gettedUser.getId());
    assertEquals(user.getName(), gettedUser.getName());
  }

  @Test
  public void testFindAll() {

    User user = new User();
    User user2 = new User();
    user2.setName("Dima");
    user.setName("Vova");


    user = entityManager.persist(user);

    User gettedUser = userRepository.findOne(user.getId());

    assertEquals(user.getId(), gettedUser.getId());
    assertEquals(user.getName(), gettedUser.getName());
  }

}
