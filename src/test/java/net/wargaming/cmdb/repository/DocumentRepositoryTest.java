package net.wargaming.cmdb.repository;

import net.wargaming.cmdb.orm.model.Document;
import net.wargaming.cmdb.orm.model.User;
import net.wargaming.cmdb.orm.repository.DocumentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class DocumentRepositoryTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private DocumentRepository documentRepository;

  @Test
  public void testFindByUserId() {
    User user = createUser();

    Document document = new Document();
    document.setType("TestType");
    document.setNumber("TestNumber");
    document.setUserId(user.getId());

    entityManager.persist(document);
    entityManager.flush();

    List<Document> docs = (List<Document>) documentRepository.findByUserId(user.getId());

    assertEquals(docs.size(), 1);
    assertEquals("TestType", docs.get(0).getType());
    assertEquals("TestNumber", docs.get(0).getNumber());
    assertEquals(user.getId(), docs.get(0).getUserId());
  }

  @Test
  public void testFindDuplicates() {
    User user = createUser();

    Document doc1 = new Document();
    doc1.setType("TestType");
    doc1.setNumber("TestNumber");
    doc1.setUserId(user.getId());

    Document doc2 = new Document();
    doc2.setType("TestType");
    doc2.setNumber("TestNumber");
    doc2.setUserId(user.getId());

    entityManager.persist(doc1);
    entityManager.persist(doc2);
    entityManager.flush();

    List<Document> docs = (List<Document>) documentRepository.findDuplicates();

    assertEquals(docs.size(), 2);
    assertEquals(docs.get(0).getType(), docs.get(1).getType());
    assertEquals(docs.get(0).getNumber(), docs.get(1).getNumber());
  }

  private User createUser() {
    User user = new User();
    user.setName("Vova");

    entityManager.persist(user);
    entityManager.flush();

    return user;
  }

}
