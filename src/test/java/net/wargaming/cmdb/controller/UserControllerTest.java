package net.wargaming.cmdb.controller;

import net.wargaming.cmdb.orm.model.User;
import net.wargaming.cmdb.orm.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static net.wargaming.cmdb.controller.Constants.ATTR_USER;
import static net.wargaming.cmdb.controller.Constants.ATTR_USERS;
import static net.wargaming.cmdb.controller.Constants.PAGE_LOGIN;
import static net.wargaming.cmdb.controller.Constants.PAGE_USER;
import static net.wargaming.cmdb.controller.Constants.PAGE_USERS;
import static net.wargaming.cmdb.controller.Constants.VIEW_USERS;
import static net.wargaming.cmdb.controller.Constants.VIEW_USER_DETAILS;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
public class UserControllerTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private UserRepository userRepository;

  @Test
  @WithMockUser
  public void getUsersPage() throws Exception {

    User userTest = new User(null, "Vova", null);
    userRepository.save(userTest);

    mvc.perform(get(PAGE_USERS))
      .andExpect(status().isOk())
      .andExpect(view().name(VIEW_USERS))
      .andExpect(model().attributeExists(ATTR_USERS))
      .andExpect(content().string(containsString("Vova")));
  }

  @Test
  @WithMockUser
  public void getUserDetail() throws Exception {

    User userTest = userRepository.save(new User(null, "Vova", null));

    mvc.perform(get(PAGE_USER + "/" + userTest.getId()))
      .andExpect(status().isOk())
      .andExpect(view().name(VIEW_USER_DETAILS))
      .andExpect(model().attributeExists(ATTR_USER))
      .andExpect(content().string(containsString("Vova")));
  }

  @Test
  @WithAnonymousUser
  public void getUsersPageAnonymous() throws Exception {

    mvc.perform(get(PAGE_USERS))
      .andExpect(status().is(302));
  }

}
