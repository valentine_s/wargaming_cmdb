package net.wargaming.cmdb.controller;

import net.wargaming.cmdb.orm.model.Document;
import net.wargaming.cmdb.orm.repository.DocumentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static net.wargaming.cmdb.controller.Constants.ATTR_DOC;
import static net.wargaming.cmdb.controller.Constants.ATTR_DOCS;
import static net.wargaming.cmdb.controller.Constants.PAGE_DOC;
import static net.wargaming.cmdb.controller.Constants.PAGE_DOCS;
import static net.wargaming.cmdb.controller.Constants.PAGE_HOME;
import static net.wargaming.cmdb.controller.Constants.VIEW_DOCS;
import static net.wargaming.cmdb.controller.Constants.VIEW_DOC_DETAILS;
import static net.wargaming.cmdb.controller.Constants.VIEW_HOME;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
public class MainControllerTest {

  @Autowired
  private MockMvc mvc;

  @Test
  @WithAnonymousUser
  public void getHomePage() throws Exception {

    mvc.perform(get(PAGE_HOME))
      .andExpect(status().isOk())
      .andExpect(view().name(VIEW_HOME))
      .andExpect(content().string(containsString("Wargaming test task")));
  }

}
