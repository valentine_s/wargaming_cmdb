package net.wargaming.cmdb.controller;

import net.wargaming.cmdb.orm.model.Document;
import net.wargaming.cmdb.orm.model.User;
import net.wargaming.cmdb.orm.repository.DocumentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static net.wargaming.cmdb.controller.Constants.ATTR_DOC;
import static net.wargaming.cmdb.controller.Constants.ATTR_DOCS;
import static net.wargaming.cmdb.controller.Constants.ATTR_USER;
import static net.wargaming.cmdb.controller.Constants.ATTR_USERS;
import static net.wargaming.cmdb.controller.Constants.PAGE_DOC;
import static net.wargaming.cmdb.controller.Constants.PAGE_DOCS;
import static net.wargaming.cmdb.controller.Constants.PAGE_USER;
import static net.wargaming.cmdb.controller.Constants.PAGE_USERS;
import static net.wargaming.cmdb.controller.Constants.VIEW_DOCS;
import static net.wargaming.cmdb.controller.Constants.VIEW_DOC_DETAILS;
import static net.wargaming.cmdb.controller.Constants.VIEW_USERS;
import static net.wargaming.cmdb.controller.Constants.VIEW_USER_DETAILS;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
public class DocumentControllerTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private DocumentRepository documentRepository;

  @Test
  @WithMockUser
  public void getDocsPage() throws Exception {

    Document document = new Document(null, "type1", "number1", null);
    documentRepository.save(document);

    mvc.perform(get(PAGE_DOCS))
      .andExpect(status().isOk())
      .andExpect(view().name(VIEW_DOCS))
      .andExpect(model().attributeExists(ATTR_DOCS))
      .andExpect(content().string(containsString("number1")));
  }

  @Test
  @WithMockUser
  public void getDocDetail() throws Exception {

    Document document = documentRepository.save(new Document(null, "type1", "number1", null));

    mvc.perform(get(PAGE_DOC + "/" + document.getId()))
      .andExpect(status().isOk())
      .andExpect(view().name(VIEW_DOC_DETAILS))
      .andExpect(model().attributeExists(ATTR_DOC))
      .andExpect(content().string(containsString("number1")));
  }

  @Test
  @WithAnonymousUser
  public void getDocsPageAnonymous() throws Exception {

    mvc.perform(get(PAGE_DOCS))
      .andExpect(status().is(302));
  }

}
