package net.wargaming.cmdb.service;

import net.wargaming.cmdb.orm.dto.UserDto;
import net.wargaming.cmdb.orm.model.Document;
import net.wargaming.cmdb.orm.model.User;
import net.wargaming.cmdb.orm.repository.DocumentRepository;
import net.wargaming.cmdb.orm.repository.UserRepository;
import net.wargaming.cmdb.orm.service.UserService;
import net.wargaming.cmdb.orm.service.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceImplTest {

  @TestConfiguration
  public static class UserServiceImplTestContextConfiguration {
    @Bean
    public UserService employeeService() {
      return new UserServiceImpl();
    }
  }

  @Autowired
  private UserService userService;

  @MockBean
  private UserRepository userRepository;

  @MockBean
  private DocumentRepository documentRepository;

  @Before
  public void setUp() {
    List<Document> docs = new ArrayList<>();
    Document document = new Document(1L, "type", "133", 2L);
    docs.add(document);

    List<User> users = new ArrayList<>();
    User user = new User(1L, "Vova", null);
    User user2 = new User(2L, "Dima", docs);

    users.add(user);
    users.add(user2);

    Mockito.when(userRepository.findOne(user.getId()))
      .thenReturn(user);

    Mockito.when(userRepository.findAll())
      .thenReturn(users);
  }

  @Test
  public void getByIdTest() {
    Long id = 1L;
    UserDto user = userService.getById(id);

    assertEquals(user.getId(), id);
  }

  @Test
  public void getAllTest() {
    List<UserDto> users = (List<UserDto>) userService.getAll();

    assertEquals(users.size(), 2);
  }

}
