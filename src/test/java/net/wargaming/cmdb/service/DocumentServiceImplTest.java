package net.wargaming.cmdb.service;

import net.wargaming.cmdb.orm.dto.DocumentDto;
import net.wargaming.cmdb.orm.model.Document;
import net.wargaming.cmdb.orm.repository.DocumentRepository;
import net.wargaming.cmdb.orm.service.DocumentService;
import net.wargaming.cmdb.orm.service.DocumentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class DocumentServiceImplTest {

  @TestConfiguration
  public static class DocumentServiceImplTestConfiguration {
    @Bean
    public DocumentService getDocumentService() {
      return new DocumentServiceImpl();
    }
  }

  @Autowired
  private DocumentService documentService;

  @MockBean
  private DocumentRepository documentRepository;

  @Before
  public void setup() {
    List<Document> docs = new ArrayList<>();
    List<Document> duplicates = new ArrayList<>();

    Document doc1 = new Document(1L, "type1", "7133", 2L);
    Document doc2 = new Document(2L, "type2", "1343", 2L);
    Document doc3 = new Document(3L, "type3", "133", 2L);
    Document doc4 = new Document(4L, "type3", "133", 2L);

    docs.add(doc1);
    docs.add(doc2);
    docs.add(doc3);
    docs.add(doc4);

    duplicates.add(doc3);
    duplicates.add(doc4);

    Mockito.when(documentRepository.findAll())
      .thenReturn(docs);

    Mockito.when(documentRepository.findByUserId(2L))
      .thenReturn(docs);

    Mockito.when(documentRepository.findDuplicates())
      .thenReturn(duplicates);

    Mockito.when(documentRepository.findOne(1L))
      .thenReturn(doc1);
  }

  @Test
  public void findByUserIdtest() {
    List<DocumentDto> docs = (List<DocumentDto>) documentService.getByUserId(2L);

    assertEquals(docs.size(), 4);
  }

  @Test
  public void findDuplicates() {
    List<DocumentDto> docs = (List<DocumentDto>) documentService.getDuplicates();

    assertEquals(docs.size(), 2);
  }

  @Test
  public void findAllTest() {
    List<DocumentDto> docs = (List<DocumentDto>) documentService.getAll();

    assertEquals(docs.size(), 4);
  }

}
